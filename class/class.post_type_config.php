<?php

/**
 * VisualizationPostType
 *
 */
class PostTypeConfig
{

    public static function make_post_type_config($post_type_label, $supports) {
        return (array(
            'label' => ucfirst($post_type_label),
            'labels' => array(
                'add_new' => 'Add New',
                'add_new_item' => 'Add New live_annotation',
                'edit_item' => 'Edit ' . $post_type_label,
                'new_item' => 'New ' . $post_type_label,
                'view_item' => 'View ' . $post_type_label,
                'search_items' => 'Search ' . $post_type_label . "s",
                'not_found' => 'No ' . $post_type_label . 's Found',
                'not_found_in_trash' => $post_type_label . 'Not Found in Trash',
            ),
            'description' => 'Reusable ' . $post_type_label . 's',
            'public' => true,
            'show_ui' => true,
            'menu_position' => 5,
            'supports' => $supports,
        ) );
    } // make_post_type_config()


} // class
?>