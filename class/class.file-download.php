<?php

define("TARGET_ID", "target_id", true);
define("DOWNLOAD_TYPE", "download_type", true);
define("POST_TYPE", "post_type", true);
define("DOWNLOAD_TYPE_ATTACHMENT", "attachment", true);
define("DOWNLOAD_TYPE_DIRECT", "direct", true);


// heavily borrowed from: https://www.ibenic.com/programmatically-download-a-file-from-wordpress/
class FileDownload
{

    private static $post_type_data_functions = array();

    public static function addPostTypeDataFunction($post_type_name, $download_callback)
    {

        self::$post_type_data_functions[$post_type_name] = $download_callback;

    }

    public static function make_download_link($post_id, $target_id, $download_type, $post_type, $link_text)
    {
        // This must be improved by using wp_nonce!
        $return_val = '<a href="' . get_permalink($post_id) . '?' . TARGET_ID . '=' . $target_id . '&' . DOWNLOAD_TYPE . '=' . $download_type . '&' . POST_TYPE . '=' . $post_type . '" download>';
        $return_val = $return_val . $link_text;
        $return_val = $return_val . '</a>';

        return ($return_val);
    }

    // must be registered with init
    public static function download_file_hook()
    {

        if (isset($_GET[TARGET_ID]) && isset($_GET[DOWNLOAD_TYPE])) {

            $download_type = $_GET[DOWNLOAD_TYPE];
            $target_id = $_GET[TARGET_ID];
            if ($download_type == DOWNLOAD_TYPE_ATTACHMENT) {
                FileDownload::send_attached_file($target_id);
            } else if ($download_type == DOWNLOAD_TYPE_DIRECT) {

                if (isset($_GET[POST_TYPE])) {

                    $post_type = $_GET[POST_TYPE];
                    FileDownload::send_buffer($post_type, $target_id);

                } else {
                    throw new Exception("The query parameters for download of type " . $download_type . " do not have a value for the " . POST_TYPE . " parameter");

                }

            } else {
                throw new Exception("Unknown download type: " . $download_type);
            }
        }
    }

// Send the file to download
    public static function send_attached_file($attachment_id)
    {
        //get filedata


        $theFile = wp_get_attachment_url($attachment_id);

        if (!$theFile) {
            return;
        }
        //clean the fileurl
        $file_url = stripslashes(trim($theFile));
        //get filename
        $file_name = basename($theFile);
        //get fileextension

        $file_info = pathinfo($file_name);

        //security check
        $fileName = strtolower($file_url);

        $whitelist = apply_filters("clo-visualization_allowed_file_types", array('png', 'gif', 'tiff', 'jpeg', 'jpg', 'bmp', 'svg', 'csv'));

        if (!in_array(end(explode('.', $fileName)), $whitelist)) {
            exit('Invalid file!');
        }

        if (strpos($file_url, '.php') == true) {
            die("Invalid file!");
        }

        $file_new_name = $file_name;
        $content_type = "";
        //check filetype

        switch ($file_info['extension']) {
            case "png":
                $content_type = "image/png";
                break;
            case "gif":
                $content_type = "image/gif";
                break;
            case "tiff":
                $content_type = "image/tiff";
                break;
            case "jpeg":
            case "jpg":
                $content_type = "image/jpg";
                break;
            case "csv":
                $content_type = "text/csv";
                break;
            default:
                $content_type = "application/force-download";
        }

        $content_type = apply_filters("clo-visualization_content_type", $content_type, $file_info['extension']);

        header("Expires: 0");
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header('Cache-Control: pre-check=0, post-check=0, max-age=0', false);
        header("Pragma: no-cache");
        header("Content-type: {$content_type}");
        header("Content-Disposition:attachment; filename={$file_new_name}");
        header("Content-Type: " . $content_type);

        readfile("{$file_url}");
        exit();
    }

    public static function send_buffer($post_type, $target_id)
    {
        if (array_key_exists($post_type, self::$post_type_data_functions)) {

            $data_callback = self::$post_type_data_functions[$post_type];
            $data_to_download = $data_callback($target_id);
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename=\"" . $target_id. ".csv\";");
            header("Content-Transfer-Encoding: binary");

            echo $data_to_download;
            exit;

        } else {
            throw new Exception("There is no buffer function for post type " . $post_type);
        }

    } // send_buffer()

}

?>