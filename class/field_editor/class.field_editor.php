<?php

include_once(plugin_dir_path(__FILE__) . "../../class/field_editor/class.field_utils.php");


class FieldEditor
{

    // We need to store and retrieve configuration instances by post-type. If you only allow for one
    // configuration object, the last plugin to be loaded overwrites any previous one.
    private static $iFieldConfigurations = array();

    public static function addFieldConfiguration($iFieldConfiguration)
    {
        self::$iFieldConfigurations[$iFieldConfiguration->getPostType()] = $iFieldConfiguration;
    }

    // the call back methods are called for all post types apparently. So the methods that
    // use this function must check for null and not care about the fact that it's null.
    private static function getFieldConfiguration($post_type)
    {

        $return_val = null;

        if (array_key_exists($post_type, self::$iFieldConfigurations)) {
            $return_val = self::$iFieldConfigurations[$post_type];
        }

        return $return_val;
    }


    /**
     * Remove the default Custom Fields meta box. Only affects the
     * content types that
     * have been activated. All inputs are sent by WP.
     * @param string $type The name of the post-type being
     * edited, e.g. 'post'
     * @param string $context Identifies a type of meta-box, e.g.
     * 'normal', 'advanced', 'side'
     * @param object $post The current post, with attributes e.g.
     * $post->ID and $post->post_name
     */
    public static function remove_default_fields($post_type, $context, $post)
    {
        $current_field_configuration = self::getFieldConfiguration($post_type);
        if (!is_null($current_field_configuration)) {

            $fields_to_remove = $current_field_configuration->getFieldsToRemove();
            $content_types_array = $fields_to_remove;
            foreach (array('normal', 'advanced', 'side') as $context) {
                foreach ($content_types_array as $content_type) {
                    remove_meta_box($content_type, $post_type, $context);
                }
            }
        } // if this is a post type that we handle

    } // remove_default_custom_fields


    public static function print_custom_fields($post, $callback_args = '')
    {

        $post_type = $post->post_type;
        $current_field_configuration = self::getFieldConfiguration($post_type);

        if (!is_null($current_field_configuration)) {

            $content_type = $callback_args['args']; // the 7th arg from add_meta_box()
            $custom_fields = $current_field_configuration->getFieldDefinitions();
            $output = '';
            foreach ($custom_fields as $field) {
                $output_this_field = '';
                $field['value'] = htmlspecialchars(get_post_meta($post->ID, $field['name'], true));
                $field['name'] = FieldUtils::make_field_id($current_field_configuration, $field['name']); // this ensures unique keys in $_POST

                switch ($field['type']) {

                    case FIELD_TYPE_CHECKBOX:
                        $output_this_field .= self::_get_checkbox_element($field);
                        break;

                    case FIELD_TYPE_DROPDOWN:
                        $output_this_field .= self::_get_dropdown_element($field);
                        break;

                    case FIELD_TYPE_TEXTAREA:
                        $output_this_field .= self::_get_textarea_element($field);
                        break;

                    case FIELD_TYPE_WYSIWYG:
                        $output_this_field .= self::_get_wysiwyg_element($field);
                        break;

                    case FIELD_TYPE_TEXT:
                    default:
                        $output_this_field .= self::_get_text_element($field);
                        break;
                }

                // optionally add description
                if ($field['description']) {
                    $output_this_field .= '<p>' . $field['description'] . '</p>';
                }
                $output .= '<div id="' . $field['name'] . '"' . ' class="' . FORM_FIELD_CLASS . '">' . $output_this_field . '</div>';
            }
            // Print the form
            print '<div class="form-wrap">';
            wp_nonce_field('update_custom_content_fields', 'custom_content_fields_nonce');
            print $output;
            print '</div>';

            // add our field scripts, mapping js object keys to element IDs and class name (decouples
            // the php code from the javascript)
            print $current_field_configuration->getAdminPageScript();

        } // if we handle this post type

    } // print_custom_fields


    /**
     * parse
     *
     * A simple parsing function for basic templating.
     *
     * @param $tpl string A formatting string containing
     * [+placeholders+]
     * @param $hash array An associative array containing keys and
     * values e.g. array('key' => 'value');
     * @return string Placeholders corresponding to the keys of
     * the hash will be replaced with the values the resulting string will be
     * returned.
     */
    private static function parse($tpl, $hash)
    {
        foreach ($hash as $key => $value) {
            $tpl = str_replace('[+' . $key . '+]', $value, $tpl);
        }
        return $tpl;
    } // parse()


    //! Private
    /**
     * The following '_get_xxx_element' functions each generate a single
     * form element.
     * @param array $data contains an associative array describing
     * how the element
     * should look with keys for name, title, description, and type.
     * @return string An HTML form element
     */
    /**
     * Note: the checked value is hard-coded to 'yes' for simplicity.
     */
    private static function _get_checkbox_element($data)
    {
        $tpl = '<input type="checkbox" name="[+name+]" id="[+name+]" value="yes" [+is_checked+] style="width: auto;"/><label for="[+name+]" style="display:inline;"><strong>[+title+]</strong></label>';
        // Special handling to see if the box is checked.
        if ($data['value'] == "yes") {
            $data['is_checked'] = 'checked="checked"';
        } else {
            $data['is_checked'] = '';
        }
        return self::parse($tpl, $data);
    } // _get_checkbox_element()

    /**
     * The dropdown is special: it requires that you supply an array of
     * options in its
     * 'options' key.
     * * The $tpl used internally here uses a custom [+options+]
     * placeholder.
     */
    private static function _get_dropdown_element($data)
    {
        // Some error messaging.
        if (!isset($data['options']) || !is_array($data['options'])) {
            return '<p><strong>Custom Content Error:</strong> No options supplied for ' . $data['name'] . '</p>';
        }
        $tpl = '<label for="[+name+]"><strong>[+title+]</strong></label><br/><select name="[+name+]" id="[+name+]">[+options+]</select>';
        $option_str = '<option value="">Pick One</option>';
        foreach ($data['options'] as $option) {
            $option = htmlspecialchars($option); // Filter the values
            $is_selected = '';
            if ($data['value'] == $option) {
                $is_selected = 'selected="selected"';
            }
            $option_str .= '<option value="' . $option . '" ' . $is_selected . '>' . $option . '</option>';
        }
        unset($data['options']); // the parse function req's a simple hash
        $data['options'] = $option_str; // prep for parsing
        return self::parse($tpl, $data);
    } // _get_dropdown_element()

//----------------------------------------------------------------
    private static function _get_text_element($data)
    {
        if (array_key_exists(FLD_DEF_KEY_SIZE, $data)) {
            $width = intval($data[FLD_DEF_KEY_SIZE]) * 10;
            $tpl = '<label for="[+name+]"><strong>[+title+]</strong></label><br/><input type="text" name="[+name+]" id="[+name+]"value="[+value+]" maxlength="[+size+]" style="width:' . $width . 'px"/><br/>';
        } else {
            $tpl = '<label for="[+name+]"><strong>[+title+]</strong></label><br/><input type="text" name="[+name+]" id="[+name+]"value="[+value+]" /><br/>';
        }

        return self::parse($tpl, $data);
    } // _get_text_element()

//----------------------------------------------------------------
    private static function _get_textarea_element($data)
    {
        $tpl = '<label for="[+name+]"><strong>[+title+]</strong></label><br/><textarea name="[+name+]" id="[+name+]" columns="30"rows="3">[+value+]</textarea>';
        return self::parse($tpl, $data);
    } // _get_textarea_element()

//----------------------------------------------------------------
    private static function _get_wysiwyg_element($data)
    {
        $tpl = '<label for="[+name+]"><strong>[+title+]</strong></label><textarea name="[+name+]" id="[+name+]" columns="30"rows="3">[+value+]</textarea>';
//        <script type="text/javascript">
//jQuery( document ).ready( function() {
//jQuery( "[+name+]" ).addClass( "mceEditor" );
//if ( typeof( tinyMCE ) == "object" && typeof( tinyMCE.
//execCommand ) == "function" ) {
//tinyMCE.execCommand( "mceAddControl", false,
//"[+name+]" );
//}
//});
//</script>
//';
        return self::parse($tpl, $data);
    } //  _get_wysiwyg_element()


    /**
     * Create the new Custom Fields meta box.
     */
    public static function create_meta_box($post_type)
    {
        $current_field_configuration = self::getFieldConfiguration($post_type);

        if (!is_null($current_field_configuration)) {
            add_meta_box($current_field_configuration->getMetaBoxId()
                , $current_field_configuration->getMetaBoxTitle()
                , 'FieldEditor::print_custom_fields', $post_type
                , 'normal'
                , 'high'
                , $post_type
            );

            // the sub-nav functionality is provided by the bird press theme. All we need to do to take advantage of it is add the
            // drop to the post configuration, here.
            add_meta_box('prfx_meta_subnav_menu', __('Subnav Menu', 'prfx-subnav'), 'prfx_meta_callback__subnav_menu', $post_type);

        } // if this is a post type that we handle

    } // create_meta_box()


    /**
     * Save the new Custom Fields values. This function reads from the
     * $_POST array
     * and stores data to the database using the update_post_meta()
     * function
     *
     * @param integer $post_id id of the post these custom fields
     * are associated with
     * @param object $post The post object
     */
    public static function save_custom_fields($post_id, $post)
    {

        $post_type = $post->post_type;
        $current_field_configuration = self::getFieldConfiguration($post_type);
        if (!is_null($current_field_configuration)) {
            // The 2nd arg here is important because there are multiple nonces on the page
            if (!empty($_POST) && check_admin_referer('update_custom_content_fields', 'custom_content_fields_nonce')) {
                $custom_fields = $current_field_configuration->getFieldDefinitions();
                foreach ($custom_fields as $field) {
                    $field_prefix = $current_field_configuration->getFieldPrefix();
                    if (isset($_POST[$field_prefix . $field['name']])) {
                        $value = trim($_POST[$field_prefix . $field['name']]);
                        // Auto-paragraphs for any WYSIWYG
                        if ($field['type'] == 'wysiwyg') {
                            $value = wpautop($value);
                        }
                        update_post_meta($post_id, $field['name'], $value);
                    } // if not set, then it's an unchecked checkbox, so blank out the value
                    else {
                        update_post_meta($post_id, $field['name'], '');
                    }
                } // iterate custom fields
            }
        } // if this is a post-type we handle
    } // save_custom_fields()

    public static function hide_editor()
    {

        global $current_screen;
        $post_type = $current_screen->post_type;

        $current_field_configuration = self::getFieldConfiguration($post_type);
        if (!is_null($current_field_configuration) && $current_field_configuration->getIsHideEditor()) {

            $css = '<style type="text/css">';
            $css .= '#wp-content-editor-container, #post-status-info, .wp-switch-editor { display: none; }';
            $css .= '</style>';

            echo $css;
        }

    }// hide_editor()


} // class
?>