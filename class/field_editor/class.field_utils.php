<?php
//include_once(plugin_dir_path(__FILE__) . "../class/classfieldutilsd_utils.php");

class FieldUtils
{

    /**
     * The field ID prefix helps ensure unique keys in the $_POST array by
     * appending his prefix to your field names. E.g. if your prefix is 'my_' and
     * your field name from the $custom_fields_for_posts array is 'field',
     * then * your form element gets generated something like <input name="my_
     * field"/>
     * and when submitted, its value would exist in $_POST['my_field']
     *
     * This prefix is *not* used as part of the meta_key when saving the
     * field
     * names to the database. If you want your fields to be
     * hidden from built-in WordPress theme functions, you can name them
     * individually
     * using an underscore "_" as the first character.
     *
     * If you omit a prefix entirely, your custom field names must steer
     * clear of
     * the built-in post field names (e.g. 'content').
     */
    public static function make_field_id($iFieldConfiguration, $field_id)
    {
        return ($iFieldConfiguration->getFieldPrefix() . $field_id);
    }

}
