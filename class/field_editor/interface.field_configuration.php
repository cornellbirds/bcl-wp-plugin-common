<?php



define("FLD_DEF_KEY_NAME", 'name');
define("FLD_DEF_KEY_TITLE", 'title');
define("FLD_DEF_KEY_DESC", 'description');
define("FLD_DEF_KEY_TYPE", 'type');
define("FLD_DEF_KEY_SIZE", 'size');
define("FLD_DEF_KEY_OPTIONS", 'options');


define("FIELD_TYPE_CHECKBOX", "checkbox", true);
define("FIELD_TYPE_DROPDOWN", "dropdown", true);
define("FIELD_TYPE_TEXTAREA", "textarea", true);
define("FIELD_TYPE_WYSIWYG", "wysiwyg", true);
define("FIELD_TYPE_TEXT", "text", true);


/// The interface is only used to impose that any class that implements it must
/// support these methods. So this file must be included in any file that implements
/// the interface. However, as a loosely typed language, PHP does not require that
/// this file be included in code that consumes the interface. In that sense, any
/// instance of a class that derives from this interface is treated just like any
/// other PHP object
///
/// The idea is that each consumer of the FieldEditor class will implement this interface in a
/// class that will be used to define how the admin page fields are bulit
interface iFieldConfiguration
{

    public function getFieldDefinitions(); // array defining the new controls to be created

    public function getFieldsToRemove(); // list of div IDs of default admin controls to remove

    public function getPostType(); // name of the custom post type

    public function getMetaBoxId(); // id of the box in which the custom fields will be contained

    public function getMetaBoxTitle(); // title of the box in which the custom fields will be contained

    public function getAdminPageScript(); // specify a script for the admin page (the module will have been loaded by functions.php)

    public function getFieldPrefix(); // arbitrary prefix for the fields

    public function getIsHideEditor(); // whether or not to hide the editor

}
?>